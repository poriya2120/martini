package main

import(
	"github.com/go-martini/martini"
	"github.com/martini-contrib/render"
	"github.com/martini-contrib/binding"
	"net/http"
)
type UserInfo struct{
	Name string `from:"name"`
	Email string `from:"email"`
}
func main(){
	m :=martini.Classic()
	m.Use(render.Renderer(render.Options{
		Layout:"layout",
	}))

	m.Get("/",func( r render.Render) {
		r.HTML(http.StatusOK,"index",nil)
	})
	// m.Get("/",func() string{
	// 	return "Hello,Welcome to the Homepage"
	// })
	m.Post("/user",binding.Bind(UserInfo{}),func( r render.Render,user UserInfo) {
		  r.HTML(http.StatusOK,"userinfo",user)
		//r.JSON(http.StatusOK,user)
	})
	m.Get("/user/:userid",func(r render.Render,p martini.Params){
		var resData struct{
			ID string
		}
		resData.ID=p["userid"]
		r.HTML(http.StatusOK,"user",resData)
		// r.JSON(http.StatusOK,resData)
	})
	m.Run()
}